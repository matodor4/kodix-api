module gitlab.com/matodor4/kodix-api

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	github.com/google/jsonapi v1.0.0
	github.com/swaggo/http-swagger v1.1.2
	github.com/swaggo/swag v1.7.4
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14 // indirect
	golang.org/x/net v0.0.0-20211020060615-d418f374d309 // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/tools v0.1.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/google/uuid v1.3.0
	golang.org/x/text v0.3.7 // indirect
)
