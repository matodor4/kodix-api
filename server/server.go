package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	v2 "gitlab.com/matodor4/kodix-api/server/v1"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/matodor4/kodix-api/model"
)

type carCruder interface {
	Create(car model.Car) (*model.Car, error)
	Read(id string) (*model.Car, error)
	Update(car model.Car) error
	Delete(id string) error
	GetAll() ([]model.Car, error)
}

type server struct {
	ctx context.Context
	mux *chi.Mux
	db  carCruder
}

func NewServer(ctx context.Context, db carCruder) *server {
	return &server{
		ctx: ctx,
		mux: chi.NewRouter(),
		db:  db,
	}
}

func (s *server) setupRoutes() {
	s.mux.Use(middleware.Recoverer)

	s.mux.Group(func(r chi.Router) {
		r.Use(func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				next.ServeHTTP(w, r)
			})
		})
		r.Mount("/", v2.InitApi(s.db))
	})
	s.mux.NotFound(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte(http.StatusText(http.StatusNotFound))); err != nil {
			fmt.Println(err)
		}
	})
	s.mux.MethodNotAllowed(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusMethodNotAllowed)
		if _, err := w.Write([]byte(http.StatusText(http.StatusMethodNotAllowed))); err != nil {
			fmt.Println(err)
		}
	})
}

func (s *server) Serve() {
	s.setupRoutes()

	port := "8080"
	server := http.Server{
		Addr:         ":" + port,
		Handler:      s.mux,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  time.Minute,
	}

	go func(s *http.Server) {
		err := s.ListenAndServe()
		log.Fatal(err)
	}(&server)
	log.Printf("start kodix server at %s port.", port)

	c := make(chan os.Signal, 1)
	go func() {
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	}()
	go func() {
		for range c {
			log.Printf("stop kodix server")
			os.Exit(1)
		}
	}()

	<-s.ctx.Done()
	log.Printf("stop kodix server")

	log.Printf("server exited properly")
}
