package v1

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/matodor4/kodix-api/storage"

	"github.com/google/jsonapi"

	"gitlab.com/matodor4/kodix-api/model"
)

type StorageMock struct {
	MockedSingleCar func() (*model.Car, error)
	MockedDeleteCar func() error
	MockedUpdateCar func() error
	MockedAllCars   func() ([]model.Car, error)
	MockedPostCar   func() (*model.Car, error)
}

func NewFakeStorage() *StorageMock {
	return &StorageMock{}
}

func (f *StorageMock) Read(_ string) (*model.Car, error) {
	return f.MockedSingleCar()
}
func (f *StorageMock) Delete(_ string) error {
	return f.MockedDeleteCar()
}

func (f *StorageMock) Update(_ model.Car) error {
	return nil
}
func (f *StorageMock) Create(_ model.Car) (*model.Car, error) {
	return f.MockedPostCar()
}

func (f StorageMock) GetAll() ([]model.Car, error) {
	return f.MockedAllCars()
}

var mockDb = NewFakeStorage()
var db = storage.NewCache()

var car = model.Car{
	Id:      "",
	Brand:   "Volvo",
	Model:   "w-100",
	Price:   999,
	Status:  "transit",
	Mileage: 666,
}

func TestHandler_post(t *testing.T) {
	requestBody := bytes.NewBuffer(nil)
	err := jsonapi.MarshalOnePayloadEmbedded(requestBody, &car)
	if err != nil {
		t.Fatal(err)
	}

	r, err := http.NewRequest(http.MethodPost, "/cars", requestBody)
	if err != nil {
		t.Fatal(err)
	}
	r.Header.Set(headerContentType, jsonapi.MediaType)

	rr := httptest.NewRecorder()
	handler := InitApi(mockDb)
	mockDb.MockedPostCar = func() (*model.Car, error) {
		return &car, nil
	}
	handler.ServeHTTP(rr, r)

	if e, a := http.StatusCreated, rr.Code; e != a {
		t.Fatalf("Expected a status of %d, got %d", e, a)
	}
}

func TestHandler_get(t *testing.T) {
	r, err := http.NewRequest(http.MethodGet, "/cars/1", nil)
	if err != nil {
		t.Error(err)
	}
	r.Header.Set(headerContentType, jsonapi.MediaType)

	rr := httptest.NewRecorder()
	handler := InitApi(mockDb)
	mockDb.MockedSingleCar = func() (*model.Car, error) {
		return &car, nil
	}
	handler.ServeHTTP(rr, r)

	if e, a := http.StatusOK, rr.Code; e != a {
		t.Fatalf("Expected a status of %d, got %d", e, a)
	}
}

func TestHandler_getAll(t *testing.T) {
	r, err := http.NewRequest(http.MethodGet, "/cars", nil)
	if err != nil {
		t.Error(err)
	}
	r.Header.Set(headerContentType, jsonapi.MediaType)

	rr := httptest.NewRecorder()
	handler := InitApi(mockDb)
	mockDb.MockedAllCars = func() ([]model.Car, error) {
		return nil, nil
	}
	handler.ServeHTTP(rr, r)

	if e, a := http.StatusOK, rr.Code; e != a {
		t.Fatalf("Expected a status of %d, got %d", e, a)
	}
}

func TestHandler_delete(t *testing.T) {
	r, err := http.NewRequest(http.MethodDelete, "/cars/car-666", nil)
	if err != nil {
		t.Error(err)
	}
	r.Header.Set(headerContentType, jsonapi.MediaType)

	rr := httptest.NewRecorder()
	handler := InitApi(mockDb)
	mockDb.MockedDeleteCar = func() error {
		return nil
	}
	handler.ServeHTTP(rr, r)

	if e, a := http.StatusNoContent, rr.Code; e != a {
		t.Fatalf("Expected a status of %d, got %d", e, a)
	}
}

func TestHandler_update(t *testing.T) {
	requestBody := bytes.NewBuffer(nil)
	err := jsonapi.MarshalOnePayloadEmbedded(requestBody, &car)
	if err != nil {
		t.Fatal(err)
	}

	r, err := http.NewRequest(http.MethodPut, "/cars", requestBody)
	if err != nil {
		t.Error(err)
	}
	r.Header.Set(headerContentType, jsonapi.MediaType)

	rr := httptest.NewRecorder()
	handler := InitApi(mockDb)
	mockDb.MockedUpdateCar = func() error {
		return nil
	}
	handler.ServeHTTP(rr, r)

	if e, a := http.StatusNoContent, rr.Code; e != a {
		t.Fatalf("Expected a status of %d, got %d", e, a)
	}
}
