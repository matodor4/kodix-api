package v1

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/google/jsonapi"
	"gitlab.com/matodor4/kodix-api/model"
)

type CarGetter interface {
	Read(id string) (*model.Car, error)
}

type CarDeleter interface {
	Delete(id string) error
}

type CarUpdater interface {
	Update(car model.Car) error
}

type CarCreator interface {
	Create(car model.Car) (*model.Car, error)
}

type CarGetterAll interface {
	GetAll() ([]model.Car, error)
}

// UpdateCar godoc
// @Summary UpdateCar
// @Description Update car in storage
// @Accept  json
// @Success 200 {object} model.Car
// @Header 200 {string} Token "qwerty"
// @Router /car [PATCH]
func UpdateCar(storage CarUpdater) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		jsonapiRuntime := jsonapi.NewRuntime().Instrument("cars.update")

		car := new(model.Car)

		if err := jsonapiRuntime.UnmarshalPayload(r.Body, car); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if err := storage.Update(*car); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusNoContent)
		w.Header().Set(headerContentType, jsonapi.MediaType)
		if err := jsonapiRuntime.MarshalPayload(w, nil); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

// DeleteCar godoc
// @Summary DeleteCar
// @Description delete car by id
// @ID delete-string-by-int
// @Accept  json
// @Success 200 {object} model.Car
// @Header 200 {string} Token "qwerty"
// @Router /car/{id} [DELETE]
func DeleteCar(storage CarDeleter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		jsonapiRuntime := jsonapi.NewRuntime().Instrument("cars.delete")
		selector := chi.URLParam(r, "id")

		if err := storage.Delete(selector); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNoContent)
		w.Header().Set(headerContentType, jsonapi.MediaType)

		if err := jsonapiRuntime.MarshalPayload(w, nil); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

// GetAll godoc
// @Summary GetAll
// @Description get all cars
// @ID delete-string-by-int
// @Accept  json
// @Success 200 {object} model.Car
// @Header 200 {string} Token "qwerty"
// @Router /cars [GET]
func GetAll(storage CarGetterAll) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		jsonapiRuntime := jsonapi.NewRuntime().Instrument("cars.list")
		list, err := storage.GetAll()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		var cars []interface{}

		for _, v := range list {
			cars = append(cars, &v)
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set(headerContentType, jsonapi.MediaType)
		if err := jsonapiRuntime.MarshalPayload(w, cars); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

// GetCar godoc
// @Summary GetCar
// @Description get a car
// @ID get-string-by-int
// @Accept  json
// @Success 200 {object} model.Car
// @Header 200 {string} Token "qwerty"
// @Router /car/{id} [GET]
func GetCar(storage CarGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		selector := chi.URLParam(r, "id")

		car, err := storage.Read(selector)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		jsonapiRuntime := jsonapi.NewRuntime().Instrument("cars.show")

		w.WriteHeader(http.StatusOK)

		w.Header().Set(headerContentType, jsonapi.MediaType)
		if err := jsonapiRuntime.MarshalPayload(w, car); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

// CreateCar godoc
// @Summary CreateCar
// @Description create a car
// @Accept  json
// @Success 200 {object} model.Car
// @Header 200 {string} Token "qwerty"
// @Router /car [POST]
func CreateCar(storage CarCreator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		jsonapiRuntime := jsonapi.NewRuntime().Instrument("cars.create")

		car := new(model.Car)

		if err := jsonapiRuntime.UnmarshalPayload(r.Body, car); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if err := car.Validate(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		returnCar, err := storage.Create(*car)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusCreated)
		w.Header().Set(headerContentType, jsonapi.MediaType)

		if err := jsonapiRuntime.MarshalPayload(w, returnCar); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}

}
