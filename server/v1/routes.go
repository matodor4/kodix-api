package v1

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/matodor4/kodix-api/model"
)

const version = "1.0.0"

const (
	headerAccept      = "Accept"
	headerContentType = "Content-Type"
)

type CarCruder interface {
	Create(car model.Car) (*model.Car, error)
	Read(id string) (*model.Car, error)
	Update(car model.Car) error
	Delete(id string) error
	GetAll() ([]model.Car, error)
}

func InitApi(db CarCruder) *chi.Mux {
	router := chi.NewRouter()

	router.Use(

		middleware.Logger,
		middleware.Compress(3, "application/json"),
		middleware.SetHeader(headerContentType, headerAccept),
		middleware.SetHeader("Version", version),
	)

	carStorage := db
	router.Route("/cars", func(r chi.Router) {

		r.Get("/{id}", GetCar(carStorage))
		r.Post("/", CreateCar(carStorage))
		r.Put("/", UpdateCar(carStorage))
		r.Delete("/{id}", DeleteCar(carStorage))
	})
	router.Get("/cars", GetAll(carStorage))
	return router
}
