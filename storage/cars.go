package storage

import (
	"fmt"
	"sync"

	"github.com/google/uuid"
	"gitlab.com/matodor4/kodix-api/model"
)

type cache struct {
	mu         sync.RWMutex
	collection map[string]model.Car
}

func (b *cache) Create(car model.Car) (*model.Car, error) {
	b.mu.Lock()
	defer b.mu.Unlock()
	car.Id = uuid.NewString()
	if _, ok := b.collection[car.Id]; !ok {
		b.collection[car.Id] = car
		return &car, nil
	}
	return nil, fmt.Errorf("can not put cat[%s] in cache ", car.Id)
}

func (b *cache) Read(id string) (*model.Car, error) {
	b.mu.Lock()
	defer b.mu.Unlock()
	item, ok := b.collection[id]
	if !ok {
		return nil, fmt.Errorf("can not find car with %s id ", id)
	}
	return &item, nil
}

func (b *cache) GetAll() ([]model.Car, error) {
	b.mu.Lock()
	defer b.mu.Unlock()
	var resultList []model.Car
	if len(b.collection) == 0 {
		return nil, fmt.Errorf("collection is empty")
	}
	for _, v := range b.collection {
		resultList = append(resultList, v)
	}
	return resultList, nil
}

func (b *cache) Update(car model.Car) error {
	b.mu.Lock()
	defer b.mu.Unlock()
	if _, ok := b.collection[car.Id]; !ok {
		return fmt.Errorf("can not update car[%s]", car.Id)
	} else {
		b.collection[car.Id] = car
	}
	return nil
}

func (b *cache) Delete(id string) error {
	b.mu.Lock()
	defer b.mu.Unlock()
	if _, ok := b.collection[id]; ok {
		delete(b.collection, id)
		return nil
	} else {
		return fmt.Errorf("can not find or delete car[%s]", id)
	}
}

func (b *cache) DeleteAll() error {
	b.mu.Lock()
	defer b.mu.Unlock()
	b.collection = make(map[string]model.Car)
	return nil
}

func NewCache() *cache {
	return &cache{
		collection: make(map[string]model.Car),
	}
}
