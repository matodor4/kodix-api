FROM golang:1.16.3-alpine3.13 as builder

RUN apk update && apk add --no-bucket git ca-certificates -f && update-ca-certificates

ENV USER=appuser
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR $GOPATH/src/backend

COPY go.mod go.sum ./

RUN go mod download
RUN go mod verify

COPY . .

RUN cd cmd/kodix && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s -X main.VERSION=${VERSION}" -o /go/bin/server

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

COPY --from=builder /go/bin/server /go/bin/server

ENV KODIX_STORAGE="cache"

USER appuser:appuser

EXPOSE 8080

ENTRYPOINT ["/go/bin/server"]