package main

import (
	"context"
	"fmt"

	"gitlab.com/matodor4/kodix-api/server"
	"gitlab.com/matodor4/kodix-api/storage"
)

// @title Kodix REST API Service
// @version 1.0
// @description API test server

// @host localhost:8080
// @BasePath /
func main() {
	fmt.Println("==================================================================")
	fmt.Println("=                  Kodix REST API Service                        =")
	fmt.Println("==================================================================")
	fmt.Println("Version: 1.0.0")

	db := storage.NewCache()
	s := server.NewServer(context.Background(), db)
	s.Serve()
}
