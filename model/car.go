package model

import (
	"encoding/json"
	"fmt"

	"gitlab.com/matodor4/kodix-api/model/errors"
)

var statusList = []string{"transit", "inStock", "sold", "delisted"}

var (
	errNoCarBrand     = fmt.Errorf("brand must is a required field")
	errNoCarModel     = fmt.Errorf("model must is a required field")
	errWrongCarStatus = fmt.Errorf("status is invalid")
	errZeroCarPrice   = fmt.Errorf("zero car price")
)

type Car struct {
	Id      string `jsonapi:"primary,cars"`
	Brand   string `jsonapi:"attr,brand"`
	Model   string `jsonapi:"attr,model"`
	Price   uint   `jsonapi:"attr,price"`
	Status  string `jsonapi:"attr,status"`
	Mileage uint   `jsonapi:"attr,mileage"`
}

func (c *Car) String() []byte {
	carByte, err := json.Marshal(c)
	if err != nil {
		return nil
	}
	return carByte
}

func (c *Car) Validate() error {
	if c.Brand == "" {
		return errors.NewCarError(errors.ErrorCode, errNoCarBrand.Error())
	}
	if c.Model == "" {
		return errors.NewCarError(errors.ErrorCode, errNoCarModel.Error())
	}
	if c.Price == 0 {
		return errors.NewCarError(errors.ErrorCode, errZeroCarPrice.Error())
	}
	if !checkStatus(statusList, c.Status) {
		return errors.NewCarError(errors.ErrorCode, errWrongCarStatus.Error())
	}
	return nil
}
func checkStatus(list []string, field string) bool {
	var find bool
	for _, v := range list {
		if v == field {
			find = true
		}
	}
	return find
}
