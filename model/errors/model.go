package errors

const ErrorCode = "400"

type CarError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func NewCarError(code, msg string) *CarError {
	return &CarError{
		Code:    code,
		Message: msg,
	}
}

func (e *CarError) Error() string {
	return e.Message
}
